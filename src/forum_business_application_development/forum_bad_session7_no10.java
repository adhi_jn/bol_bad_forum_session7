/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forum_business_application_development;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 *
 * @author Adhi Joyo Negoro
 */
public class forum_bad_session7_no10 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
            throws Exception {
        File temp = File.createTempFile("pattern", ".suffix");
        temp.deleteOnExit();
        BufferedWriter out = new BufferedWriter(new FileWriter(temp));
        out.write("aString");
        System.out.println("temporary file created:");
        out.close();
    }

}
