/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forum_business_application_development;

import java.io.File;

/**
 *
 * @author Adhi Joyo Negoro
 */
public class forum_bad_session7_no8 {

    /**
     * @param args the command line arguments
     */
    // TODO code application logic here
    public static long getFileSize(String filename) {
        File file = new File(filename);
        if (!file.exists() || !file.isFile()) {
            System.out.println("File doesn\'t exist");
            return -1;
        }
        return file.length();
    }

    public static void main(String[] args) {
        long size = getFileSize("c:/ajn/java.txt");
        System.out.println("Filesize in bytes: " + size);
    }
}
